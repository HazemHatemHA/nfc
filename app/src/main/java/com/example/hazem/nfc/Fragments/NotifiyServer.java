package com.example.hazem.nfc.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.hazem.nfc.R;

/**
 * Created by Hazem on 2/18/2018.
 */

public class NotifiyServer extends Fragment implements FragmentsProperties {
    private EditText ID_studant;
    private EditText note_context;
    private Button btn_search;
    private Button btn_send;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View v = inflater.inflate(R.layout.notifiy_server,container,false);
         ID_studant = (EditText)v.findViewById(R.id.ID_studant);
         note_context = (EditText) v.findViewById(R.id.note_context);
         Button btn_search =(Button)v.findViewById(R.id.btn_search);
         Button btn_send=(Button)v.findViewById(R.id.btn_send);
        getChildFragmentManager().beginTransaction()
                .replace(R.id.studant_info_fragment, new Studant_info(),"STUDANT_INFO_").commit();
        return v;
    }

    @Override
    public String getNameFragment() {
        return "محضر ضبط";
    }
}
