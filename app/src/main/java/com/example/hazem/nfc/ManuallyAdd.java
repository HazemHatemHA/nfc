package com.example.hazem.nfc;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.example.hazem.nfc.Fragments.Studant_info;

import java.util.zip.Inflater;

/**
 * Created by Hazem on 2/18/2018.
 */

public class ManuallyAdd extends MainActivity {

    private EditText ID_studant;
    private Button btn_search;
    private CheckBox check_att;
    private Button btn_att;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.isMainActivity = false;
        super.onCreate(savedInstanceState);
        super.vpPager.setVisibility(View.INVISIBLE);


        RelativeLayout rl = (RelativeLayout) findViewById(R.id.content_main);
        View result = LayoutInflater.from(this)
                .inflate(R.layout.manually_add, null);
        rl.addView(result);

        ID_studant = (EditText)findViewById(R.id.ID_studant);
        check_att = (CheckBox)findViewById(R.id.check_att);
        btn_search = (Button)findViewById(R.id.btn_search);
        btn_att = (Button)findViewById(R.id.btn_att);

        btn_att.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent MainActivity = new Intent(ManuallyAdd.this, com.example.hazem.nfc.MainActivity.class);
                startActivity(MainActivity);
            }
        });

       getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.studant_info_fragment,new Studant_info(),"STUDANT_INFO_manually_add").commit();

    }
}
