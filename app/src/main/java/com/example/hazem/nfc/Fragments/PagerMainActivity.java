package com.example.hazem.nfc.Fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

/**
 * Created by Hazem on 2/19/2018.
 */

public class PagerMainActivity extends FragmentPagerAdapter {
    private ArrayList<FragmentsProperties> myfragments;
    public PagerMainActivity(FragmentManager fm , ArrayList<FragmentsProperties> myfragments) {
        super(fm);
        this.myfragments = myfragments;
    }

    @Override
    public Fragment getItem(int position) {
        return (Fragment)myfragments.get(position);
    }

    @Override
    public int getCount() {
        return myfragments == null ? 0 : myfragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return myfragments.get(position).getNameFragment();
    }
}
