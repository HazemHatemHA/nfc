package com.example.hazem.nfc.entity;

/**
 * Created by Hazem on 2/19/2018.
 */

public class InfoExam {

    public String exam_place;
    public String exam_date;
    public String exame_time;
    public String exam_subject;

    @Override
    public String toString() {
        return "InfoExam{" +
                "exam_place='" + exam_place + '\'' +
                ", exam_date='" + exam_date + '\'' +
                ", exame_time='" + exame_time + '\'' +
                ", exam_subject='" + exam_subject + '\'' +
                '}';
    }
}
