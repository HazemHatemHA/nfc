package com.example.hazem.nfc.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.hazem.nfc.R;

/**
 * Created by Hazem on 2/18/2018.
 */

public class ExamRoom extends Fragment implements FragmentsProperties{
    private TextView text_view_Num_tudents_attending;
    private TextView text_view_Num_enrolled_students;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.exam_room,container,false);
        text_view_Num_tudents_attending = (TextView)v.findViewById(R.id.text_view_Num_tudents_attending);
        text_view_Num_enrolled_students = (TextView)v.findViewById(R.id.text_view_Num_enrolled_students);
        getChildFragmentManager().beginTransaction()
                .replace(R.id.studant_info_fragment,new Studant_info(),"STUDANT_INFO").commit();
        return v;
    }
    @Override
    public String getNameFragment() {
        return "قاعة الاختبار";
    }
}
