package com.example.hazem.nfc.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.hazem.nfc.R;
import com.example.hazem.nfc.entity.InfoExam;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by Hazem on 2/19/2018.
 */

public class RowTable extends RecyclerView.Adapter<RowTable.RowViewHolder> {
    private ArrayList<InfoExam> myData;
    private Context context;

    public RowTable(Context context,ArrayList<InfoExam>myData){
        this.context = context;
        this.myData = myData;
    }
    @Override
    public RowViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.row_recy,parent,false);
        return new RowViewHolder(v);
    }


    @Override
    public void onBindViewHolder(RowViewHolder holder, int position) {
        holder.exam_date.setText(myData.get(position).exam_date);
        holder.exam_subject.setText(myData.get(position).exam_subject);
        holder.exam_time.setText(myData.get(position).exame_time);
        holder.exame_palce.setText(myData.get(position).exam_place);
    }

    @Override
    public int getItemCount() {
        return myData.size();
    }

    public class RowViewHolder extends RecyclerView.ViewHolder{
        TextView exame_palce;
        TextView exam_date;
        TextView exam_time;
        TextView exam_subject;

        public RowViewHolder(View itemView) {
            super(itemView);
            exam_date = (TextView)itemView.findViewById(R.id.exam_date);
            exam_time = (TextView)itemView.findViewById(R.id.exam_hour);
            exame_palce = (TextView)itemView.findViewById(R.id.exam_place);
            exam_subject = (TextView)itemView.findViewById(R.id.exam_subject);
        }
    }
}
