package com.example.hazem.nfc.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hazem.nfc.Adapters.RowTable;
import com.example.hazem.nfc.Data.LoadData;
import com.example.hazem.nfc.R;
import com.example.hazem.nfc.entity.InfoExam;

import java.util.ArrayList;

/**
 * Created by Hazem on 2/18/2018.
 */

public class TableTeacher extends Fragment implements FragmentsProperties{
    private RecyclerView recy_rows;
    private ArrayList<InfoExam> myData;
    private RowTable myAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.table_teacher,container,false);
        myData = new ArrayList<InfoExam>();
        LoadData.LoadTable(myData);
        recy_rows = (RecyclerView)v.findViewById(R.id.recy_rows);

        recy_rows.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(v.getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recy_rows.setLayoutManager(llm);
         myAdapter = new RowTable(v.getContext(),myData);
        recy_rows.setAdapter(myAdapter);

        return v;
    }

    @Override
    public String getNameFragment() {
        return "جدول المراقب";
    }
}
