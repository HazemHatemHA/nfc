package com.example.hazem.nfc.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hazem.nfc.R;

/**
 * Created by Hazem on 2/19/2018.
 */

public class Studant_info extends Fragment {

    public ImageView studant_image;
    public TextView studant_name;
    public TextView studant_coll;
    public TextView studant_spci;
    public TextView ID_studant;

    private static Studant_info instance;
    public static Studant_info getInstance(){
        if(instance == null) instance = new Studant_info();

        return  instance;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.studant_info,container,false);
        studant_image = (ImageView)v.findViewById(R.id.studant_image);
        studant_name = (TextView)v.findViewById(R.id.studant_name);
        studant_coll = (TextView)v.findViewById(R.id.studant_coll);
        studant_spci = (TextView)v.findViewById(R.id.studant_spci);
        ID_studant = (TextView)v.findViewById(R.id.ID_studant);

        return v;
    }
}
